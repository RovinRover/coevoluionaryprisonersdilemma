/**
 * Created by levi on 11/1/17.
 */

val random = Options.random

fun randomNode(useFull : Boolean, depth : Int) : AbstractNode {
    if(useFull){
        if(depth < Options.d){
            return randomFunctionNode(useFull, depth + 1)
        }
        else{
            return randomTerminalNode()
        }
    }
    else{
        if(depth == Options.d){
            return randomTerminalNode()
        }
        if(random.nextBoolean()){
            return randomTerminalNode()
        }
        else {
            return randomFunctionNode(useFull, depth + 1)
        }
    }
}

fun randomTerminalNode() : TerminalNode {
    val competitor = if(random.nextBoolean()) Competitor.SELF else Competitor.OPPONENT
    return TerminalNode(competitor, random.nextInt(Options.k) + 1)
}

fun randomFunctionNode(useFull: Boolean, depth: Int) : AbstractNode {
    when(random.nextInt(4)){
        0 -> return AndNode(randomNode(useFull, depth + 1), randomNode(useFull, depth + 1))
        1 -> return OrNode(randomNode(useFull, depth + 1), randomNode(useFull, depth + 1))
        2 -> return XorNode(randomNode(useFull, depth + 1), randomNode(useFull, depth + 1))
        else -> return NotNode(randomNode(useFull, depth + 1))
    }
}

