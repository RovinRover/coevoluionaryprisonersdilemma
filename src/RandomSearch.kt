/**
 * Created by levi on 11/1/17.
 */

fun randomSearch() {
    val titForTat = GPTree(TerminalNode(Competitor.OPPONENT, 1))
    var globalSolution : GPTree? = null

    for(run in 1..Options.runs) {
        println("Starting Run $run")
        var localSolution : GPTree? = null
        Logger.logHeader(run)

        for(evals in 1..Options.evals){
            val tree = GPTree.grow()
            tree.calcFitness(titForTat)
            if(tree.fitness > (localSolution?.fitness ?: -1f)){
                Logger.log(evals, tree.fitness, 0f)
                localSolution = tree
                if(tree.fitness > (globalSolution?.fitness ?: -1f)){
                    globalSolution = tree
                }
            }
        }
    }
    Logger.logSolution(globalSolution.toString())
}