/**
 * Created by levi on 10/30/17.
 */
enum class Choice(val bool: Boolean) {
    DEFECT(false), COOPERATE(true)
}

fun Boolean.toChoice(): Choice {
    return if (this) Choice.COOPERATE else Choice.DEFECT
}

fun calcPayoff(self: Choice, opponent: Choice): Int = when(self){
    Choice.COOPERATE -> when(opponent){
        Choice.COOPERATE -> 3
        Choice.DEFECT -> 5
    }
    Choice.DEFECT -> when(opponent){
        Choice.COOPERATE -> 0
        Choice.DEFECT -> 1
    }
}