/**
 * Created by levi on 10/30/17.
 */
class Memory(private val self: MutableList<Choice>, private  val opponent: MutableList<Choice>) {

    fun add(selfChoice: Choice, opponentChoice: Choice) {
        self.add(0, selfChoice)
        opponent.add(0, opponentChoice)

        self.removeAt(self.lastIndex)
        opponent.removeAt(opponent.lastIndex)
    }

    companion object{
        fun random(): Memory{
            return Memory(MutableList(Options.k) {Options.random.nextBoolean().toChoice()},
                          MutableList(Options.k) {Options.random.nextBoolean().toChoice()})
        }
    }

    //One Indexed
    operator fun get(competitor: Competitor, index : Int) = when(competitor){
        Competitor.SELF -> self[index-1]
        Competitor.OPPONENT -> opponent[index-1]
    }

    //Overrides !
    //Returns a memory where opponent and self are swapped
    //Shallow copy. Do not change
    operator fun not(): Memory = Memory(opponent, self)

    //Debug Print Statement
    fun print(){
        print("Self:\t\t")
        self.forEach { print(it.toString() +  (if(it == Choice.DEFECT) "\t" else "") + ",\t" )}
        print("\nOpponent:\t")
        opponent.forEach { print(it.toString() +  (if(it == Choice.DEFECT) "\t" else "") + ",\t") }
        print("\n\n")

    }
}