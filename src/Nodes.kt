/**
 * Created by levi on 10/30/17.
 */
sealed class AbstractNode {
    var parent : AbstractNode? = null
    var depth : Int = (parent?.depth ?: 0) + 1
    abstract fun calc(memory: Memory): Boolean
    abstract fun getNodes(): List<AbstractNode>
    abstract fun replace(value: AbstractNode, replacement: AbstractNode)
    abstract fun updateDepth(depth: Int)
    abstract fun updateParent(parent: AbstractNode?)
    abstract fun copy() : AbstractNode
    override abstract fun toString(): String
}

sealed class AbstractBinaryNode(var leftChild: AbstractNode, var rightChild: AbstractNode) : AbstractNode(){
    abstract val functionName: String

    final override fun getNodes() = List(1){this} + leftChild.getNodes() + rightChild.getNodes()

    final override fun replace(value: AbstractNode, replacement: AbstractNode) {
        if(leftChild == value){
            leftChild = replacement
            return
        }
        if(rightChild == value){
            rightChild = replacement
            return
        }
        throw(Error("Value not found"))
    }

    final override fun updateDepth(depth: Int) {
        this.depth = depth
        if(depth + 1 == Options.d){
            if(leftChild !is TerminalNode){
                leftChild = randomTerminalNode()
                leftChild.parent = this
            }
            if(rightChild !is TerminalNode){
                rightChild = randomTerminalNode()
                rightChild.parent = this
            }
        }
        leftChild.updateDepth(depth + 1)
        rightChild.updateDepth(depth + 1)
    }

    final override fun updateParent(parent: AbstractNode?) {
        this.parent = parent
        leftChild.updateParent(this)
        rightChild.updateParent(this)
    }

    final override fun toString() = functionName + " " + leftChild.toString() + rightChild.toString()

}

class TerminalNode(val competitor: Competitor, val index: Int): AbstractNode() {
    override fun calc(memory: Memory) = memory[competitor, index].bool

    override fun getNodes() = List(1){this}

    override fun replace(value: AbstractNode, replacement: AbstractNode) {
        throw(Error("Should not happen"))
    }

    override fun updateDepth(depth: Int) {
        this.depth = depth
    }

    override fun updateParent(parent: AbstractNode?) {
        this.parent = parent
    }

    override fun copy() = TerminalNode(competitor, index)

    override fun toString() = competitor.toString() + index.toString() + " "
}

class AndNode(leftChild : AbstractNode, rightChild : AbstractNode): AbstractBinaryNode(leftChild, rightChild) {

    override fun calc(memory: Memory): Boolean {
        return leftChild.calc(memory) and rightChild.calc(memory)
    }

    override val functionName = "AND"

    override fun copy() = AndNode(leftChild.copy(), rightChild.copy())

}

class OrNode(leftChild : AbstractNode, rightChild : AbstractNode): AbstractBinaryNode(leftChild, rightChild) {

    override fun calc(memory: Memory): Boolean {
        return leftChild.calc(memory) or rightChild.calc(memory)
    }

    override val functionName = "OR"

    override fun copy() = OrNode(leftChild.copy(), rightChild.copy())

}

class XorNode(leftChild : AbstractNode, rightChild : AbstractNode): AbstractBinaryNode(leftChild, rightChild){

    override fun calc(memory: Memory): Boolean {
        return leftChild.calc(memory) xor rightChild.calc(memory)
    }

    override val functionName = "XOR"

    override fun copy() = XorNode(leftChild.copy(), rightChild.copy())
}

class NotNode(var child : AbstractNode): AbstractNode(){
    override fun calc(memory: Memory): Boolean {
        return child.calc(memory).not()
    }

    override fun getNodes() = List(1){this} + child.getNodes()

    override fun replace(value: AbstractNode, replacement: AbstractNode) {
        if(child== value){
            child = replacement
            return
        }
        throw(Error("Value not found"))
    }

    override fun updateDepth(depth: Int) {
        this.depth = depth
        if(depth + 1 == Options.d){
            if(child !is TerminalNode){
                child = randomTerminalNode()
                child.parent = this
            }
        }
        child.updateDepth(depth + 1)
    }

    override fun updateParent(parent: AbstractNode?) {
        this.parent = parent
        child.updateParent(this)
    }

    override fun copy() = NotNode(child.copy())

    override fun toString() = "NOT " + child.toString()
}