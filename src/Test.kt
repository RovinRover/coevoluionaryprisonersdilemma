/**
 * Created by levi on 11/14/17.
 */

fun main(args: Array<String>) {
    val myList = listOf(1, 2, -3, 0, 10, -7)
    print(myList.sortedBy { -it })
}