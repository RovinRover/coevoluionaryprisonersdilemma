/**
 * Created by levi on 11/1/17.
 */
class GPTree(val root: AbstractNode) {
    var fitness = 0.0f

    fun calcChoice(memory: Memory) = root.calc(memory).toChoice()

    fun getNodes() = root.getNodes()

    fun getRandomSubNode() : AbstractNode? {
        val nodes = getNodes().toMutableList()
        nodes.remove(root)
        if(nodes.size == 0){
            return null
        }
        return nodes[Options.random.nextInt(nodes.size)]
    }

    fun subTreeMutate() {
        val node = getRandomSubNode() ?: return

        val parent = node.parent ?: throw(NullPointerException())

        parent.replace(node, randomNode(false, node.depth + 1))
    }

    fun subTreeCrossover(rhs: GPTree) {
        val lhsNode = this.getRandomSubNode()
        val rhsNode = rhs.getRandomSubNode()


        //Check for trees with no sub nodes
        if(lhsNode == null || rhsNode == null ){
            return
        }

        val lhsParent = lhsNode.parent!!
        val rhsParent = rhsNode.parent!!

        lhsParent.replace(lhsNode, rhsNode)
        rhsParent.replace(rhsNode, lhsNode)

        lhsNode.parent = rhsParent
        rhsNode.parent = lhsParent

        lhsNode.updateDepth(rhsParent.depth + 1)
        rhsNode.updateDepth(lhsParent.depth + 1)
    }

    fun calcFitness(opponent: GPTree): Float{
        var payoff = 0.0f
        val memory = Memory.random()

        for(i in 1..2*Options.k){
            val selfChoice = calcChoice(memory)
            val opponentChoice = opponent.calcChoice(!memory)

            memory.add(selfChoice, opponentChoice)
        }

        for(i in 1..Options.l-(Options.k*2)){
            val selfChoice = calcChoice(memory)
            val opponentChoice = opponent.calcChoice(!memory)

            memory.add(selfChoice, opponentChoice)
            payoff += calcPayoff(selfChoice, opponentChoice)
        }
        return payoff/(Options.l-(Options.k*2)) - getNodes().size * Options.p
    }

    fun copy() : GPTree {
        val tree = GPTree(root.copy())
        tree.root.updateParent(null)
        tree.root.updateDepth(1)
        return tree
    }

    override fun toString() = root.toString()

    companion object{
        fun grow() : GPTree {
            val tree = GPTree(randomNode(false, 1))
            tree.root.updateParent(null)
            tree.root.updateDepth(1)
            return tree
        }
        fun full() : GPTree {
            val tree = GPTree(randomNode(true, 1))
            tree.root.updateParent(null)
            tree.root.updateDepth(1)
            return tree
        }
    }
}

