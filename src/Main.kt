
/**
 * Created by levi on 10/30/17.
 */

fun main(args: Array<String>){
    parseConfig(args[0])
    Logger.open()
    when(Options.searchType){
        0 -> randomSearch()
        1 -> evolutionarySearch()
    }
    Logger.close()
}