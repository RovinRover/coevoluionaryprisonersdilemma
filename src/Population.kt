import com.sun.org.apache.bcel.internal.generic.POP
import javax.swing.text.html.Option

/**
 * Created by levi on 11/14/17.
 */
class Population(val population : MutableList<GPTree>) {

    fun parentSelection() = when(Options.parentSelection) {
        0 -> fitnessSelection()
        else -> overSelection()
    }

    fun fitnessSelection() : Population{
        var fitnessSum = 0f
        population.forEach {
            fitnessSum += it.fitness
        }
        return Population(MutableList(Options.lambda){
            var randomNum = Options.random.nextFloat()*fitnessSum
            var returnTree : GPTree? = null
            population.forEach {
                randomNum -= it.fitness
                if(randomNum <= 0){
                    returnTree = it.copy()
                }
            }
            returnTree ?: population.last()
        })
    }

    fun overSelection() : Population{
        val cutoff = (Options.mu * Options.x).toInt()
        val numOfMoreFitParents = (Options.lambda*.80).toInt()
        population.sortBy { -it.fitness }
        val moreFitGroup = population.slice(0..cutoff)
        val lessFitGroup = population.slice((cutoff + 1)..population.lastIndex)

        val moreFitParents = MutableList(numOfMoreFitParents) {
            moreFitGroup[Options.random.nextInt(moreFitGroup.size)].copy()
        }

        val lessFitParents = MutableList(Options.lambda-numOfMoreFitParents){
            lessFitGroup[Options.random.nextInt(moreFitGroup.size)].copy()
        }

        moreFitParents += lessFitParents
        return Population(moreFitParents)
    }

    fun survivalSelection() = when(Options.survivalSelection){
        0 -> truncation()
        else -> kTournament()
    }

    fun truncation() : Population{
        population.sortBy { -it.fitness }
        return Population(population.slice(0..Options.mu-1).toMutableList())
    }

    fun kTournament() : Population{
        val winners = mutableListOf<GPTree>()
        for(i in 1..Options.mu){
            val tournament = mutableListOf<GPTree>()
            for(j in 1..Options.kTournament){
                tournament.add(population.removeAt(Options.random.nextInt(population.size)))
            }
            tournament.sortBy { -it.fitness }
            winners.add(tournament.removeAt(0))
            population += tournament
        }
        return Population(winners)
    }

    fun recombine() {
        val iterator = population.iterator()
        while(iterator.hasNext()){
            iterator.next().subTreeCrossover(iterator.next())
        }

    }

    fun selfMutate() {
        population.forEach {
            if(Options.random.nextFloat() < Options.mutationRate) {
                it.subTreeMutate()
            }
        }
    }

    fun calcFitness() {
        population.forEach {
            var avgFitness = 0f
            val opponents = population.toMutableList()
            opponents.remove(it)
            for(i in 1..Options.coevolutionaryFitnessSample){
                avgFitness += it.calcFitness(opponents.removeAt(Options.random.nextInt(opponents.size)))
            }
            avgFitness /= Options.coevolutionaryFitnessSample
            it.fitness = avgFitness
        }
    }

    fun avgFitness() : Float {
        var average = 0f
        population.forEach {
            average += it.fitness
        }
        average /= population.size
        return average
    }

    fun maxFitness() = population.maxBy { it.fitness }

    val size get() = population.size

    operator fun plusAssign(rhs : Population){
        population += rhs.population
    }

    operator fun get(i: Int) = population[i]

    companion object{
        fun halfAndHalf() = Population(MutableList(Options.mu){
            if(Options.random.nextBoolean()){
                GPTree.full()
            }
            else{
                GPTree.grow()
            }
        })
    }
}