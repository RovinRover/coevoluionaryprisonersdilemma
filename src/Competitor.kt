/**
 * Created by levi on 11/1/17.
 */
enum class Competitor {
    SELF{ override fun toString() = "P" },
    OPPONENT{ override fun toString() = "O" };

    abstract override fun toString(): String
}