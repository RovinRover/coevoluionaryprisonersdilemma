import java.util.*

/**
 * Created by levi on 10/30/17.
 */
object Options {
    //the sequence length specifying the number of iterations to play
    var l = 0

    //the maximum length of the agent memory
    var k = 1

    //the maximum tree depth, where d = 0 would be a tree consisting of just a root which
    //would be a single terminal node
    var d = 0

    var seed: Long = System.currentTimeMillis()
    var random = Random(seed)
    var runs = 1
    var evals = 1
    var logFilePath = ""
    var solutionFilePath = ""
    var mu = 1

    //Note: lambda should be even for recombination
    var lambda = 1

    //0 = Fitness, 1 = Over-Selection
    var parentSelection = 0

    //0 = Truncation, 1 = k-Tournament
    var survivalSelection = 0

    //Parsimony pressure
    var p = 0f

    //Variable used in over-selection
    var x = 0f

    //0 = number of evals, 1 = no change in fitness for n evals
    var termination = 0

    //Number of generations of no change
    var nTermination = 0

    //0 = Random Search, 1 = Evolutionary Search
    var searchType = 0

    //0 = Plus, 1 = Comma
    var survivalStrategy = 0

    //Int between 1 and (mu + lambda - 1)
    var coevolutionaryFitnessSample = 1

    var mutationRate = 0f
    var kTournament = 1
}