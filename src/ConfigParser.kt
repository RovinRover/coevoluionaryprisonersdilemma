import java.io.File
import java.util.*

/**
 * Created by levi on 10/30/17.
 */

fun parseConfig(filename: String){
    val file = File(filename)
    file.forEachLine {
        val words = it.split(Regex(":\\s*"))
        when (words[0]){
            "l" -> Options.l = words[1].toInt()
            "k" -> Options.k = words[1].toInt()
            "d" -> Options.d = words[1].toInt()
            "seed" -> {
                try {
                    Options.seed = words[1].toLong()
                    Options.random = Random(Options.seed)
                } catch(e: NumberFormatException) {}
            }
            "runs" -> Options.runs = words[1].toInt()
            "evals" -> Options.evals = words[1].toInt()
            "logFilePath" -> Options.logFilePath = words[1]
            "solutionFilePath" -> Options.solutionFilePath = words[1]
            "mu" -> Options.mu = words[1].toInt()
            "lambda" -> Options.lambda = words[1].toInt()
            "parentSelection" -> Options.parentSelection = words[1].toInt()
            "survivalSelection" -> Options.survivalSelection = words[1].toInt()
            "p" -> Options.p = words[1].toFloat()
            "x" -> Options.x = words[1].toFloat()
            "termination" -> Options.termination = words[1].toInt()
            "nTermination" -> Options.nTermination = words[1].toInt()
            "searchType" -> Options.searchType = words[1].toInt()
            "survivalStrategy" -> Options.survivalStrategy = words[1].toInt()
            "coevolutionaryFitnessSample" -> Options.coevolutionaryFitnessSample = words[1].toInt()
            "mutationRate" -> Options.mutationRate = words[1].toFloat()
            "kTournament" -> Options.kTournament = words[1].toInt()
            else -> println("${words[0]} is not a settable option")
        }
    }
}
