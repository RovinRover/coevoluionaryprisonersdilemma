import com.sun.xml.internal.ws.policy.spi.PolicyAssertionValidator
import java.io.File

/**
 * Created by levi on 11/2/17.
 */
object Logger {
    private val logFile = File(Options.logFilePath).printWriter()
    private val solutionFile = File(Options.solutionFilePath).printWriter()

    fun open(){
        logFile.println("Result Log\n")
        logConfigHeader()
    }

    fun log(evals: Int, avgFitness: Float, bestFitness: Float){
        logFile.println("$evals\t$avgFitness\t$bestFitness")
    }

    fun logHeader(run: Int){
        logFile.println("\nRun $run")
    }

    fun logAbsoluteFitness(fitness: Float){
        logFile.println("Absolute Fitness " + fitness.toString())
    }

    fun logConfigHeader(){
        logFile.println("l: ${Options.l}")
        logFile.println("k: ${Options.k}")
        logFile.println("d: ${Options.d}")
        logFile.println("seed: ${Options.seed}")
        logFile.println("runs: ${Options.runs}")
        logFile.println("evals: ${Options.evals}")
        logFile.println("logFilePath: ${Options.logFilePath}")
        logFile.println("solutionFilePath: ${Options.solutionFilePath}")
        logFile.println("mu: ${Options.mu}")
        logFile.println("lambda: ${Options.lambda}")
        logFile.println("parentSelection: ${Options.parentSelection}")
        logFile.println("survivalSelection: ${Options.survivalSelection}")
        logFile.println("p: ${Options.p}")
        logFile.println("x: ${Options.x}")
        logFile.println("termination: ${Options.termination}")
        logFile.println("nTermination: ${Options.nTermination}")
        logFile.println("searchType: ${Options.searchType}")
        logFile.println("survivalStrategy: ${Options.survivalStrategy}")
        logFile.println("coevolutionaryFitnessSample: ${Options.coevolutionaryFitnessSample}")
        logFile.println("mutationRate: ${Options.mutationRate}")
        logFile.println("kTournament: ${Options.kTournament}")
    }

    fun logSolution(solution: String){
        solutionFile.println(solution)
    }

    fun close(){
        logFile.close()
        solutionFile.close()
    }
}