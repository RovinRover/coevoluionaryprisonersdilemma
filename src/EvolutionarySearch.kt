
/**
 * Created by levi on 11/16/17.
 */

fun evolutionarySearch(){
    val titForTat = GPTree(TerminalNode(Competitor.OPPONENT, 1))
    var bestSolution : GPTree? = null

    for(run in 1..Options.runs){

        println("Starting run $run")
        Logger.logHeader(run)
        var population = Population.halfAndHalf()
        population.calcFitness()
        var evals = Options.mu * Options.coevolutionaryFitnessSample
        Logger.log(evals, population.avgFitness(), population.maxFitness()!!.fitness)
        var lastChange = 0

        while(!terminate(evals, lastChange)){
            val nextGeneration = population.parentSelection()
            nextGeneration.recombine()
            nextGeneration.selfMutate()

            when(Options.survivalStrategy) {
            //Plus
                0 -> population += nextGeneration
            //Comma
                else -> population = nextGeneration
            }

            population.calcFitness()
            evals += population.size * Options.coevolutionaryFitnessSample

            population = population.survivalSelection()

            val localBest = population.maxFitness()
            Logger.log(evals, population.avgFitness(), localBest!!.fitness)

            if(localBest.fitness> bestSolution?.fitness ?: -1f){
                bestSolution = localBest
            }
            else{
                lastChange++
            }
        }
        val absoluteFittest = population.population.maxBy {
            val fitness = it.calcFitness(titForTat)
            it.fitness = fitness
            fitness
        }
        Logger.logAbsoluteFitness(absoluteFittest!!.fitness)
    }

    Logger.logSolution(bestSolution.toString())
}

fun terminate(evals: Int, lastChange: Int) = when(Options.termination) {
    0 -> evals >= Options.evals
    else -> lastChange >= Options.nTermination
}